import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};
		int i=0 ;
		printBoard(board);
		while (true) {
			System.out.print("Player 1 enter row number:");
			int row = reader.nextInt();
			System.out.print("Player 1 enter column number:");
			int col = reader.nextInt();
			if (((row > 3) || (row < 1)) || ((col > 3) || (col < 1))) {
				System.out.println("this row or col cant be enter.try again.");
				continue;
			}
			else if (board[row - 1][col - 1] == 'X')  {
				System.out.println("This area already occupied.try again.");
				continue;
			}
			else if (board[row - 1][col - 1] == 'O') {
				System.out.println("This area already occupied.try again.");
				continue;
			}
			else {
				board[row - 1][col - 1] = 'X';
				printBoard(board);
				i=i+1;
			}	if (i==9) {
				reader.close();
				break;
				}

			while (true) {
				System.out.print("Player 2 enter row number:");
				int row2 = reader.nextInt();
				System.out.print("Player 2 enter column number:");
				int col2 = reader.nextInt();
				if (((row2 > 3) || (row2 < 1)) || ((col2 > 3) || (col2 < 1))) {
					System.out.println("this kind of row or col number cant be enter.try again.");
					continue;
				}
				else if ((board[row2 - 1][col2 - 1] == 'X')) {
					System.out.println("this area already occupied.try again.");
					continue;
				}
				else if ((board[row2 - 1][col2 - 1] == 'O')) {
					System.out.println("this area already occupied.try again.");
					continue;
				}
				else {
					board[row2 - 1][col2 - 1] = 'O';
					printBoard(board);
					i=i+1;
					break;
				}
			}


		}

		//reader.close();

	}
	public static void printBoard ( char[][] board){
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}
}